#include <map_planner/map_planner.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <bits/stdc++.h>

using namespace amrita2019;
bool isValid(bool [][10000], int, int, int, int);
bool area_map[10000][10000];
// unsigned char array[10000][10000];
int m,n;
unsigned char ch;
typedef std::pair <double , std::pair<float , float>> astarBase;
std::vector<amrita2019::Pose> nodeList;
bool astar(const Pose& start, const Pose& goal, std::vector<Pose>& nodeList, bool starVisited[]);
void printNodeList();

void Planner::initialize(std::string map_name) {
	/*
	This function reads the map file and initializes the presence of blocks.
	Each pixel is identified with thresholding to be an active/inactive node.
	Nodes (Cells) are numbered vertically as m*j+i, where,
	m = total number of rows
	j = column iterator
	i = row iterator
	nodeNum, x, y, active (thresholding) are pushed to the struct Pose.

	This method is equivalent to:
	1. Read and Parse the pixel values from the file
	2. Push node numbers, coordinates and tags their activity (T/F)
	3. Creates nodeList (vector of <Pose> struct) and pushes the adjacency list of each node
	
	Args:
	map_name: string; Name assigned in main()

	Returns: ~
	*/
	int threshold = 250;
	int row=0,col=0,node=0,i,j,x,y,q;
    std::string map_path = ros::package::getPath("map_planner") + "/data" + "/" + map_name;
    ROS_INFO("Initializing the planner with %s", map_path.c_str());
    std::ifstream infile;
    infile.open(map_path, std::ios::binary);
    std::stringstream ss;
    std::string inputline= "";
    getline(infile,inputline);
  	if(inputline.compare("P5") != 0)
  		std::cerr << "Version Error" << std::endl;
  	else std::cout << "Version : " << inputline << std::endl;
  	// Second line : comment
  	getline(infile,inputline);
  	std::cout << "Comment : " << inputline << std::endl;
  	// Continue with a stringstream
  	// ss << infile.rdbuf();
  	// Third line : size
  	infile >> m >> n;
  	std::cout << n << " columns and " << m << " rows" << std::endl;
  	getline(infile,inputline);
  	infile >> q; 
  	std::cout<<"Maximum Value: "<<q<<std::endl;
  	// std::cout<<"Data is given below: \n";
  	//area_map contains boolean value to define active/inactive nodes
  	// Following lines : data

  	for(int col = 0; col < m; col++){
    	for (int row = 0; row < n; row++){
    		infile >> ch;
    		// array[row][col] = ch;
    		//detecting inactive nodes by thresholding
    		if((int)ch < threshold){
    			area_map[row][col] = 0;
    			// std::cout<<" ";
    		}else{
    			area_map[row][col] = 1;
    			// std::cout<<"*";
    		}
    		// std::cout<<(int)array[row][col]<<"\t";
    	}
    	// std::cout<<std::endl;
  	}
  	//threshold value for gray level set as variable "threshold"
	for(int j=0;j<m;j++){
		for(int i=0;i<n;i++){
			int node=m*j+i;
			Pose p = Pose();
			if(area_map[i][j]==1){
				p.active=true;
			}//setting value to false and making it an inactive node
    		else{
    			p.active=false;
    		}    			
			p.x=(float)i;
			p.y=(float)j;
			p.nodeNum=node;
			nodeList.push_back(p);
		}
	}
	int count=0;
	for(int j=0;j<m;j++){
		for(int i=0;i<n;i++){
			if(area_map[i][j]) {
				//finding neighbour nodes
				if(isValid(area_map, i-1, j, m , n)) {
					//Top Node
					nodeList[count].adjlist.push_back({i-1, j});
				}
				if(isValid(area_map, i, j-1, m , n)) {
					//Left Node
					nodeList[count].adjlist.push_back({i, j-1});
				}
				if(isValid(area_map, i, j+1, m , n)) {
					//Right Node
					nodeList[count].adjlist.push_back({i, j+1});
				}
				if(isValid(area_map, i+1, j, m , n)) {
					//Bottom Node
					nodeList[count].adjlist.push_back({i+1, j});
				}
				if(isValid(area_map, i-1, j-1, m , n)) {
					//TL Node
					nodeList[count].adjlist.push_back({i-1, j-1});
				}
				if(isValid(area_map, i-1, j+1, m , n)) {
					//TR Node
					nodeList[count].adjlist.push_back({i-1, j+1});
				}
				if(isValid(area_map, i+1, j-1, m , n)) {
					//BL Node
					nodeList[count].adjlist.push_back({i+1, j-1});			
				}
				if(isValid(area_map, i+1, j+1, m , n)) {
					//BR Node
					nodeList[count].adjlist.push_back({i+1, j+1});			
				}
			}count++;
		}
	}
			// printNodeList();

  	infile.close();
}

bool DFSPlanner::makePlan(const Pose& start, const Pose& goal, std::vector<Pose>& nodeList) { 
	/*
	DFS Plan identifies if the path exists between the start and goal node.
	Traverses all the cells present in the pathway and checks if the goal node exists
	in any node's adjacency.
	Makes use of Stack to push and pop element while traversing.
	Checks if the popped element is equal to destination coordinates.
	
	This method is equivalent to:
	visited[] - marks the node number as visited
	parentMap[] - marks the node number of previous element (Helps in back-tracking)
	printed[] - array to verify if nodes visited and printed are same
	1. Initialize visited[], parentMap[], printed[], stack, path (vector)
	2. Push either start node or from adjacency of parent node
	3. Pop if stack isn't empty and compare with destination
	4. Mark visited[popped] = true, Pop from stack, check adjacency list of popped element
	5. Print backward path from goal to start
	
	Args:
	start: Pose struct(x,y,nodeNum)
	goal: Pose struct(x,y,nodeNum)
	nodeList: vector of Pose

	Returns: (Type: boolean) : states existence of path 

	*/
	std::cout<<"\nDFS algorithm path\n";
	bool visited[m*n];
	int parentMap[m*n];
	int printed[m*n];
    for(int i = 0; i < m*n; i++)
    {
        visited[i] = false;
        parentMap[i] = -1;
        printed[i] = -1;
    }
	std::stack<int> s;
	std::vector<int> path;
    s.push(start.nodeNum);
    while(!s.empty())
    {
    	int popped = s.top();
    	if(popped==goal.nodeNum){
    		std::cout<<"\nPath exists\n";
    		int current = goal.nodeNum;
    		int count = 0;
    		while(current != -1){
    			//parentMap initialised to -1 if not visited
    			//path printed until parent element is marked unvisited
    			printed[current] = 0;
    			std::cout<<"{"<<nodeList[current].x<<","<<nodeList[current].y<<"}--";
    			count++;
    			current = parentMap[current];
    		}
    		int ct = 0;
    		for (int i = 0; i < m*n; i++)
    		{
    			if(printed[i]==0){
    				ct++;
    			}
    		}
    		// std::cout<<"\n"<<count<<","<<ct;
    		return true;
    	}
    	visited[popped] = true;
    	s.pop();
    	for(std::pair<int,int> p: nodeList[popped].adjlist)
    	{
    		int n = m * p.second+ p.first;
    		if(visited[n]==false){
    			s.push(n);
    			parentMap[n] = popped;
    		}
    	}
	}
return false;
}

bool AStarPlanner::makePlan(const Pose& start, const Pose& goal, std::vector<Pose>& nodeList) {
	/*
	Computes heurestic function and angular movement for determining the path using A*
	starVisited stores the flag if the node number is visited to track movement.
	Calls: astar() passing start node, end node, nodeList and starVisited[]
	Here: g is the cost of travelling from current node to adjacent node
		  h is the cost computed from chosen adjacent node to end node (heurestic)
	This method is equivalent to:
	1. Initialize starVisited
	2. Push thetaGH vector with theta, g value and h value
	3. Calls astar(start, goal, nodeList, starVisited)

	Args:
	start: Pose struct(x,y,nodeNum)
	goal: Pose struct(x,y,nodeNum)
	nodeList: vector of Pose
	
	Returns: (Type: boolean) states existence of path
	*/
	bool starVisited[m*n];
    for(int i = 0; i < m*n; i++)
    {
        starVisited[i] = false;
    }
    int count = 0;
    for(int j=0;j<m;j++){
		for(int i=0;i<n;i++){

			//               Top
			// TL	 +135    +90     +45  TR
			//		  
			//Left   +180  -*Robot*->  0   Right
			//
			// BL	-135    -90     -45   BR
			// 		       Bottom
			
			if(area_map[i][j]) {
				//finding neighbour nodes
				if(isValid(area_map, i-1, j, m , n)) {
					//Top Node					
					float theta = 90;
					float g = 0.01;
					double h = sqrt(((goal.x - (i-1))*(goal.x - (i-1))) + ((goal.y - (j))*(goal.y - (j))));
					nodeList[count].thetaGH.push_back({theta,{g,h}});//Turns Left
				}
				if(isValid(area_map, i, j-1, m , n)) {
					//Left Node					
					float theta = 180;
					float g = 0.01;
					double h = sqrt(((goal.x - (i))*(goal.x - (i))) + ((goal.y - (j-1))*(goal.y - (j-1))));
					nodeList[count].thetaGH.push_back({theta,{g,h}});
				}
				if(isValid(area_map, i, j+1, m , n)) {
					//Right Node					
					float theta = 0;
					float g = 0.01;
					double h = sqrt(((goal.x - (i))*(goal.x - (i))) + ((goal.y - (j+1))*(goal.y - (j+1))));
					nodeList[count].thetaGH.push_back({theta,{g,h}});
				}
				if(isValid(area_map, i+1, j, m , n)) {
					//Bottom Node				
					float theta = (-1*90);
					float g = 0.01;
					double h = sqrt(((goal.x - (i+1))*(goal.x - (i+1))) + ((goal.y - (j))*(goal.y - (j))));
					nodeList[count].thetaGH.push_back({theta,{g,h}});
				}
				if(isValid(area_map, i-1, j-1, m , n)) {
					//TL Node
					float theta = 135;
					float g = 0.01414;//Diagonal Movement
					double h = sqrt(((goal.x - (i-1))*(goal.x - (i-1))) + ((goal.y - (j-1))*(goal.y - (j-1))));
					nodeList[count].thetaGH.push_back({theta,{g,h}});
				}
				if(isValid(area_map, i-1, j+1, m , n)) {
					//TR Node
					float theta = 45;
					float g = 0.01414;
					double h = sqrt(((goal.x - (i-1))*(goal.x - (i-1))) + ((goal.y - (j+1))*(goal.y - (j+1))));
					nodeList[count].thetaGH.push_back({theta,{g,h}});
				}
				if(isValid(area_map, i+1, j-1, m , n)) {
					//BL Node
					float theta = (-1*135);
					float g = 0.01414;
					double h = sqrt(((goal.x - (i+1))*(goal.x - (i+1))) + ((goal.y - (j-1))*(goal.y - (j-1))));
					nodeList[count].thetaGH.push_back({theta,{g,h}});				
				}
				if(isValid(area_map, i+1, j+1, m , n)) {
					//BR Node
					float theta = (-1*45);
					float g = 0.01414;
					double h = sqrt(((goal.x - (i+1))*(goal.x - (i+1))) + ((goal.y - (j+1))*(goal.y - (j+1))));
					nodeList[count].thetaGH.push_back({theta,{g,h}});				
				}
			}count++;
		}
	}
	// printNodeList();

   	std::cout<<"\nA star algorithm path\n";
   	if(astar(start, goal, nodeList, starVisited)){
   		std::cout<<"\nPath Found\n";
   		return true;
   	}
   	else{
   		std::cout<<"\nPath Not Found\n";
   		return false;
   	}

}


int flag = 0;
bool astar(const Pose& start, const Pose& goal, std::vector<Pose>& nodeList, bool starVisited[]){
	/*
	Recursive function that computes the a star path.
	Prints traversal node at every recursion until the goal node is reached.
	Adjacent node is chosen based on f value.
	Here, f = g + h, sum of costs of movement to adjacent node and to final node
	This method is equivalent to:
	1. Check if start and goal node are same
	2. Traverse through the adjacency list of current node (start)
	3. Create a priority queue defined with astarBase that is min_queue
	4. Push f and adjacent node coordinates to min_queue
	5. Pop the top element which is the minimum "f" node
	6. Pass new coordinates as start node

	Args:
	start: Pose struct(x,y,nodeNum)
	goal: Pose struct(x,y,nodeNum)
	nodeList: vector of Pose
	starVisited[]: bool 
	
	Retuns: (Type: bool) states existence of path
	*/
	Pose newStart = Pose();
	std::priority_queue<astarBase, std::vector<astarBase>, std::greater<astarBase> > min_queue;
	starVisited[start.nodeNum] = true;
	int a = start.x;
	int b = start.y;
	if((start.x == goal.x) && (start.y == goal.y)){
		std::cout<<"{"<<start.x<<","<<start.y<<"} \n ";
		std::cout<<"\nPath exists"<<std::endl;
		flag = 1;
		return true;
		// exit(EXIT_SUCCESS);
	}
	else{
	    std::cout<<"{"<<start.x<<","<<start.y<<"}--";

	    for(int l=0; l<nodeList[start.nodeNum].adjlist.size();l++){
	    	if(flag==1){break;}
	    	//checking if visited
	    	int p = nodeList[start.nodeNum].adjlist[l].first;
			int q = nodeList[start.nodeNum].adjlist[l].second;
			double f = nodeList[start.nodeNum].thetaGH[l].second.first + nodeList[start.nodeNum].thetaGH[l].second.second;
			//f = g + h
	    	if((starVisited[m*q+p] == false)){ 
	    		min_queue.push(std::make_pair(f , std::make_pair(p,q)));
    		}
    		else{
    			continue;
    		}
	    }
	    astarBase top = min_queue.top();
	    newStart.x = top.second.first;
    	newStart.y = top.second.second;
    	newStart.nodeNum = m*newStart.y + newStart.x;
		astar(newStart, goal, nodeList, starVisited);
	}
	return false;
}


//Utility Functions

bool isValid(bool area_map[][10000],int i, int j, int m, int n){
	/*
	Boundary Check
	Args:
	area_map[][]: bool
	i,j : int (stores coordinate details)
	m,n: int (total rows and columns)
	Returns: (Type bool) Validates if not outside image dimension
	*/
	if((i>=0)&&(i<m)&&(j>=0)&&(j<n)) {
		return area_map[i][j];
	}else{
	return false;
	}
}

void printNodeList(){
	//Prints nodeList (vector of Pose)
	for(int a=0; a<nodeList.size(); a++){
        std::cout<<"{ "<<"Node=>"<<"Number="<<nodeList[a].nodeNum<<" , "<<nodeList[a].x<<","<<nodeList[a].y<<" }-- "<<nodeList[a].active<<" ";
        std::cout<<" Size="<<nodeList[a].adjlist.size()<<"\tadjlist: ";
        for(int b=0; b<nodeList[a].adjlist.size(); b++){
            std::cout<<"{"<<nodeList[a].adjlist[b].first<<","<<nodeList[a].adjlist[b].second<<"}";
            std::cout<<"theta:"<<nodeList[a].thetaGH[b].first<<" g: "<<nodeList[a].thetaGH[b].second.first<<" h: "<<nodeList[a].thetaGH[b].second.second<<" ";
        }
        std::cout<<std::endl;
    }
}


























// int flag = 0;
// bool dfsplan(const Pose& start, const Pose& goal, std::vector<Pose>& nodeList, bool visited[]){ 
 //    int nodeIterator = (m*start.x)+start.y;
	// int currentNode = (m*start.y)+start.x;
	// visited[currentNode] = true;
	// int a = start.x;
	// int b = start.y;
 //    	if((start.x == goal.x) && (start.y == goal.y)){
	// 		std::cout<<"{"<<nodeList[nodeIterator].x<<","<<nodeList[nodeIterator].y<<"} \n ";
	// 		std::cout<<"\nPath exists"<<std::endl;
	// 		flag = 1;
	// 		return true;
 //    	}
 //    	else{
	// 	    std::cout<<"{"<<nodeList[nodeIterator].x<<","<<nodeList[nodeIterator].y<<"}--";
	// 	    Pose newStart = Pose();
	// 	    for(int l=0; l<nodeList[nodeIterator].adjlist.size();l++){
	// 	    	//checking if visited
	// 	    	if((visited[(m*nodeList[nodeIterator].adjlist[l].second)+nodeList[nodeIterator].adjlist[l].first] == 0)){
	// 	    		if(flag==1){break;}   
	// 	    		newStart.x = nodeList[nodeIterator].adjlist[l].first;
	//     			newStart.y = nodeList[nodeIterator].adjlist[l].second;
	//     			return dfsplan(newStart, goal, nodeList, visited);
	//     		}
	//     		else{
	//     			continue;
	//     		}
	// 	    }	
 //    	}
 //    return false;

// }
