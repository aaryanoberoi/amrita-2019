#include <map_planner/map_planner.h>
#include<thread>
using namespace amrita2019;

int main (int argc, char *argv[]) {

    ROS_INFO("Planner executable called");
    std::string map_name = "maze.pgm";
    int start_a=0,start_b=0,end_a=0,end_b=0;
    amrita2019::AStarPlanner astar; // construct astar planner
    amrita2019::DFSPlanner dfs; // construct dfs planner
    dfs.initialize(map_name);
    std::cout<< "\nEnter the starting coordinates as x and y" << std::endl;
    std::cin >> start_a >> start_b;
    std::cout<< "\nEnter the ending coordinates as x and y" << std::endl;
    std::cin >> end_a >> end_b;
    Pose start = Pose();
    start.x = start_a;
    start.y = start_b;
    start.nodeNum = m*start_b+start_a;
    Pose goal = Pose();
    goal.x = end_a;
    goal.y = end_b;
    goal.nodeNum = m*end_b+end_a;
    if(nodeList[start.nodeNum].active==false){
        std::cout<<"Invalid/Inactive Starting coordinates"<<std::endl;
    }
    else if(nodeList[goal.nodeNum].active==false){
        std::cout<<"Invalid/Inactive Ending coordinates"<<std::endl;
    }
    else{
        // std::thread t1(&DFSPlanner::makePlan, start, goal, nodeList);
        // t1.join();
        // std::thread t2(&AStarPlanner::makePlan, start, goal, nodeList);
        // t2.join();
        dfs.makePlan(start, goal, nodeList);
        astar.makePlan(start, goal, nodeList);
    }   
    //Printing Adjacency List
    // for(int a=0; a<nodeList.size(); a++){
    //     std::cout<<"{ "<<"Node=>"<<"Number="<<nodeList[a].nodeNum<<" , "<<nodeList[a].x<<","<<nodeList[a].y<<" }";
    //     std::cout<<" Size="<<nodeList[a].adjlist.size()<<"\tadjlist: ";
    //     for(int b=0; b<nodeList[a].adjlist.size(); b++){
    //         std::cout<<"{"<<nodeList[a].adjlist[b].first<<","<<nodeList[a].adjlist[b].second<<"} ";
    //     }
    //     std::cout<<std::endl;
    // }       
    // return 0;
}