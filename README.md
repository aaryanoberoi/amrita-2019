# amrita-2019
Submission by Aaryan Oberoi

# Shortest Path using DFS and A*

Robot Navigation is an important field of research in Mobile Robotics. In many used cases, there may be obstacles present in the path and the robot is required to find the shortest path from the source to the detination. Many research studies have led to formulation of algorithms to identify the shortest path between two differently located points. In order to make the problem simpler, we divide the map into a grid. Each cell in the grid represents a node. This project has an input image of a maze file stored in ```map_planner/data/maze.pgm```. Each pixel denotes a node and the Portable Gray Map Image has each pixel valued from 0 (Black) to 255 (White). Each pixel data is read and stored as a 2D array. With thresholding being performed, another array stores the active/inactive state of the node (pixel).
Each node is numbered vertically downwards and this helps to populate the adjacency list as the data of each node is stored in the form of vector. 
``` 
 _________________
|_0_|_110_|____
|_1_|_111_|__
|_2_|_112_|
|_3_|  |  |
|_4_| \ / |
|_5_|  v
|

```
Pose structure has been defined in map_planner.h as follows:
```
struct Pose:
float x;
float y;
int nodeNum;
bool active;
std::vector<std::pair<int, int> > adjlist;
std::vector<std::pair<float, std::pair<float, double>> > thetaGH;

```
* x - x coordinate of node
* y - y coordinate of node
* nodeNum  - node number
* active   - state of the node
* adjlist  - adjacency list of each node as vector of pair
* thetaGH  - corresponding theta, g and h values of each adjacent node
* g - cost of movement to adjacent node
* h - total cost (heurestic) from chosen node to final node

```
std::vector<amrita2019::Pose> nodeList;
``` 
nodeList is defined as a vector of struct Pose and populates the data in above format of each node. 

### Depth First Search
DFS is a simple recursive algorithm that follows the concept of backtracking. The traversing takes place until all the child nodes are visited and then backtracking takes place. Backtracking means, when you're going forward and there are no more child nodes to visit on the current path, you move back along the same path and traverse through other unvisited nodes. All the nodes on one path are visited before the second path is chosen. In this project, the recursive state of DFS is implemented with the help of Stack. 
![alt text](https://www.tutorialspoint.com/data_structures_algorithms/images/stack_representation.jpg)

Stack is an ordered collection where the addition and extraction of data takes place at the same end. The ordering principle is LIFO (Last-in-First-Out) where newer items are added on the top and older items are available in the bottom.
* Push starting Node and all its adjacent nodes to the Stack
* Pop the node and compare with destination node
* Mark the node as visited to avoid infinite loop

Useful in traversing the entire graph but may consume more time finding the path to the destination. The path may be very long and is not based on any parameters.
## A * Search
A star is known to be a smart algorithm to find the required destination. It uses the heurestic to guide itself. On traversing in the adjacency list of the current node, it check the node which has the lowest f=g+h function value and expands in that direction. To compute heurestic, either Manhattan distance or Eucledian distance may be used. This project makes use of Eucledian distance for heurestic computation.
``` h = sqrt((current_cell.x - goal.x)^2 - (current_cell. - goal.y)^2)``` 
![alt text](http://mnemstudio.org/ai/path/images/a-star1b.gif)

Priority Queue is used for computing the lowest f for the node in adjacency of current node.
It is an extension of queue with following properties.
1) Every item has a priority associated with it.
2) An element with high priority is dequeued before an element with low priority.
3) If two elements have the same priority, they are served according to their order in the queue.

        typedef std::pair <double , std::pair<float , float>> astarBase;
        std::priority_queue<astarBase, std::vector<astarBase>, std::greater<astarBase> > min_queue;

astarBase is defined in the program to store f (g+h) value as double and pair of coordinates of adjacenct nodes. min_queue helps put the lowest cost f element on top. New coordinates of start point are passed to the recusive call based on the selection by min_queue.

### How is the project better?
* No values have been hard coded and the system should work well even for larger image files
* All important data sets are stored as vectors and hence will consume memory as per the size required. No pre-allocation of memory is done.
* G and H values are pre-computed and stored in nodeList and this will help save processing time during the actual performance of task.

### Installation

1. Install ROS Kinetic (choose Desktop-Full Install under point 1.4): [http://wiki.ros.org/kinetic/Installation/Ubuntu](http://wiki.ros.org/kinetic/Installation/Ubuntu)

2. Install build tools:

        sudo apt-get install python-wstool python-catkin-tools clang-format-3.8

3. Re-use or create a catkin workspace:

        export CATKIN_WS=~/ws_catkin
        mkdir -p $CATKIN_WS/src
        cd $CATKIN_WS/src

4. Download the required repositories and install any dependencies:

        git clone https://github.com/rapyuta-robotics/amrita-2019.git
        wstool init .
        wstool update 
            rosdep update
        rosdep install --from-paths . --ignore-src --rosdistro kinetic

5. Configure and build the workspace:

        cd ..
        catkin config --extend /opt/ros/kinetic --cmake-args -DCMAKE_BUILD_TYPE=Release
        catkin build

6. Source the workspace.

        source devel/setup.bash
        
7. Optional:  If you would like to source the workspace for all terminals, add the below line to ~/.bashrc

        source <path_to_your_workspace>/devel/setup.bash

### It's RUN time

        catkin build

        rosrun map_planner map_planner_node

## Future Work

1. Parsing the input file with SDL library
--> This will help correct the present dataset and will make the system more reliable for path computation

2. Adding Exception Handling for Boundary checks 
--> This will help in handling erroneous input values outside of the maze boundary

3. Use of threads to perform operations concurrently

4. Test Cases to check the program run for different data inputs
--> This will help in identifying the loopholes and make changes to source code as per the error

## Known Errors
1. Image Parsing Issue
2. A star recursion problem for Far Distant Nodes

## Acknowledgments

* Parsing: https://stackoverflow.com/questions/8126815/how-to-read-in-data-from-a-pgm-file-in-c
* Google Style Guide: https://google.github.io/styleguide/cppguide.html
* DFS: https://gist.github.com/smihir031/3b00125074a66e2102d190742c83254f/
* Priority Queue: https://www.geeksforgeeks.org/priority-queue-of-pairs-in-c-ordered-by-first/
* Special Thanks to Rapyuta Robotics Team! I wouldn't have learned these algorithms and Data Structures concepts otherwise. :)

